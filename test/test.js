const assert = require('assert');
const tasks = require('../src');

describe('07-yield-tasks', function() {

    it('getFibonacciSequence should return the Fibonacci sequence', () => {

        var expected = [
            0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181,
            6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269,
            2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169
        ];

        var index = 0;
        for(let num of tasks.getFibonacciSequence()) {
            assert.equal(
                num,
                expected[index++],
                `Sequence mismatch at index no ${index}: `
            );
            if (index>=expected.length) break;
        }
        if (index<expected.length) assert.fail(index, expected.length,`sequence length should be equal to ${expected.length}`);

    });


    it('depthTraversalTree should return the sequence of tree nodes in depth-first order', () => {

        /*
         *     source tree (root = 1):
         *
         *            1
         *          / | \
         *         2  6  7
         *        / \     \            =>    { 1, 2, 3, 4, 5, 6, 7, 8 }
         *       3   4     8
         *           |
         *           5
         */

        var node1 = { n:1 }, node2 = { n:2 }, node3 = { n:3 }, node4 = { n:4 }, node5 = { n:5 }, node6 = { n:6 }, node7 = { n:7 }, node8 = { n:8 };
        node1.children = [ node2, node6, node7 ];
        node2.children = [ node3, node4 ];
        node4.children = [ node5 ];
        node7.children = [ node8 ];
        var expected = [ node1, node2, node3, node4, node5, node6, node7, node8 ];
        var index = 0;
        for(let num of tasks.depthTraversalTree(node1)) {
            if (index>=expected.length) assert.fail(index, expected.length,`sequence length should be equal to ${expected.length}`);
            assert.equal(
                num.n,
                expected[index++].n,
                `Sequence mismatch at index no ${index}: `
            );
        }
        if (index<expected.length) assert.fail(index, expected.length,`sequence length should be equal to ${expected.length}`);
    });

    const MAX_NODE_COUNT = 100000;

    function createDeepTree() {
        var root = { n: MAX_NODE_COUNT };
        for(var i=MAX_NODE_COUNT-1; i>0; i--) {
            root = { n : i, children : [ root ] };
        }
        return root;
    }

    function createWideTree() {
        var root = { n: 1, children: [] };
        for(var i=2; i<=MAX_NODE_COUNT; i++) {
            root.children.push({ n: i });
        }
        return root;
    }

    it('depthTraversalTree should process a deep tree', () => {
        var root = createDeepTree();
        var index = 1;
        for(let node of tasks.depthTraversalTree(root)) {
            if (index > MAX_NODE_COUNT) assert.fail(index, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
            assert.equal(
                node.n,
                index,
                `Sequence mismatch at index no ${index}: `
            );
            index++;
        }
        if (index-1<MAX_NODE_COUNT) assert.fail(index-1, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
    });

    it('depthTraversalTree should process a wide tree', () => {
        var root = createWideTree();
        var index = 1;
        for(let node of tasks.depthTraversalTree(root)) {
            if (index > MAX_NODE_COUNT) assert.fail(index, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
            assert.equal(
                node.n,
                index,
                `Sequence mismatch at index no ${index}: `
            );
            index++;
        }
        if (index-1<MAX_NODE_COUNT) assert.fail(index-1, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
    });


    it('breadthTraversalTree should return the sequence of tree nodes in depth-first order', () => {

        /*
         *     source tree (root = 1):
         *
         *            1
         *          / | \
         *         2  3  4
         *        / \     \            =>    { 1, 2, 3, 4, 5, 6, 7, 8 }
         *       5   6     7
         *           |
         *           8
         */

        var node1 = { n:1 }, node2 = { n:2 }, node3 = { n:3 }, node4 = { n:4 }, node5 = { n:5 }, node6 = { n:6 }, node7 = { n:7 }, node8 = { n:8 };
        node1.children = [ node2, node3, node4 ];
        node2.children = [ node5, node6 ];
        node4.children = [ node7 ];
        node6.children = [ node8 ];
        var expected = [ node1, node2, node3, node4, node5, node6, node7, node8 ];
        var index = 0;
        for(let num of tasks.breadthTraversalTree(node1)) {
            if (index>=expected.length) assert.fail(null,null,`sequence length should be equal to ${expected.length}`);
            assert.equal(
                num.n,
                expected[index++].n,
                `Sequence mismatch at index no ${index}: `
            );
        }
        if (index<expected.length) assert.fail(index, expected.length,`sequence length should be equal to ${expected.length}`);
    });


    it('breadthTraversalTree should process a deep tree', () => {
        var root = createDeepTree();
        var index = 1;
        for(let node of tasks.breadthTraversalTree(root)) {
            if (index > MAX_NODE_COUNT) assert.fail(index, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
            assert.equal(
                node.n,
                index,
                `Sequence mismatch at index no ${index}: `
            );
            index++;
        }
        if (index-1<MAX_NODE_COUNT) assert.fail(index-1, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
    });

    it('breadthTraversalTree should process a wide tree', () => {
        var root = createWideTree();
        var index = 1;
        for(let node of tasks.breadthTraversalTree(root)) {
            if (index > MAX_NODE_COUNT) assert.fail(index, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
            assert.equal(
                node.n,
                index,
                `Sequence mismatch at index no ${index}: `
            );
            index++;
        }
        if (index-1<MAX_NODE_COUNT) assert.fail(index-1, MAX_NODE_COUNT,`sequence length should be equal to ${MAX_NODE_COUNT}`);
    });


    it('mergeSortedSequences should merge two sorted sequences into one sorted sequence', () => {
        const ITEMS_COUNT = 500;

        var odds = function* () {
            for(var i=1; true; i+=2) yield i;
        };
        var evens = function* () {
            for(var i=2; true; i+=2) yield i;
        };
        var expected = 1;
        var count = 0;
        for(let value of tasks.mergeSortedSequences(odds, evens)) {
            assert.equal(
                value,
                expected++
            );
            count++;
            if (count==ITEMS_COUNT) break;
        }
        assert.equal(count, ITEMS_COUNT);

        var zero = function* () { yield 0; }
        expected = 0;
        count = 0;
        for(let value of tasks.mergeSortedSequences(zero, evens)) {
            assert.equal(
                value,
                expected
            );
            expected +=2;
            count++;
            if (count == ITEMS_COUNT) break;
        }
        assert.equal(count, ITEMS_COUNT);


        var minus1 = function* () { yield -1; }
        expected = -1;
        count = 0;
        for(let value of tasks.mergeSortedSequences(odds, minus1)) {
            assert.equal(
                value,
                expected
            );
            expected +=2;
            count++;
            if (count == ITEMS_COUNT) break;
        }
        assert.equal(count, ITEMS_COUNT);

    });
});
