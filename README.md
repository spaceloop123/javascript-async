# JavaScript Async

## Tasks

#### Fibonacci
Return the Fibonacci sequence.
For example:
```js
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...
```
Write your code in `src/index.js` within an appropriate function `getFibonacciSequence`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Depth Traversal Tree
Traverse a tree using the [breadth-first strategy](https://en.wikipedia.org/wiki/Breadth-first_search).

Each node have child nodes in node.children array.

The leaf nodes do not have 'children' property.

For example:
```js
source tree (root = 1):

        1
      / | \
     2  3  4
    / \     \            =>    { 1, 2, 3, 4, 5, 6, 7, 8 }
   5   6     7
       |
       8
```
Write your code in `src/index.js` within an appropriate function `breadthTraversalTree`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Merge sorted sequences
Merge two yield-style sorted sequences into the one sorted sequence.
The result sequence consists of sorted items from source iterators.
For example:
```js
[ 1, 3, 5, ... ], [2, 4, 6, ... ]  => [ 1, 2, 3, 4, 5, 6, ... ]
[ 0 ], [ 2, 4, 6, ... ]  => [ 0, 2, 4, 6, ... ]
[ 1, 3, 5, ... ], [ -1 ] => [ -1, 1, 3, 5, ...]
```
Write your code in `src/index.js` within an appropriate function `mergeSortedSequences`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: #
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-async/  
4. Go to folder `javascript-async`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-async)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
